﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class LightFade : MonoBehaviour
{
    Light sceneLight;
    float startingLight;
	void Start ()
    {
        sceneLight = this.GetComponent<Light>();
        startingLight = sceneLight.intensity;
        sceneLight.intensity = 0;
        sceneLight.DOIntensity(startingLight, 1.5f);
	}
	
	
}
