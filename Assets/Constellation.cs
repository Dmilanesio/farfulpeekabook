﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class Constellation : MonoBehaviour
{
    public bool canDraw;
    public GameObject[] anchors;
    public GameObject[] points;
    Vector3[] pointPositions;
    private LineRenderer lr;
    int step = 0;
    public ParticleSystem stars;
    public Renderer[] farfulMesh;
	// Use this for initialization
	void Start ()
    {
        lr = this.GetComponent<LineRenderer>();
        pointPositions = new Vector3[points.Length];
        for (int i = step; i < pointPositions.Length; i++)
        {
            pointPositions[i] = points[i].transform.position;
        }
        lr.positionCount = 19;
        lr.SetPositions(pointPositions);
        step++;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(Input.GetKeyDown(KeyCode.Q))
        {
            StartConstellation();
        }
    
        if(step < lr.positionCount - 1 && canDraw)
        {
            Draw();
        }
        else
        {
            stars.Stop();
        }
    }
    public void StartConstellation()
    {
        canDraw = true;
        stars.Play();
        for (int i = 0; i < farfulMesh.Length; i++)
        {
            farfulMesh[i].material.DOFade(1, 2f);
        }
    }
    public void Draw()
    {
        for (int i = step; i < pointPositions.Length; i++)
        {
            points[i].transform.position = Vector3.Lerp(points[i].transform.position, anchors[step + 1].transform.position, 7f * Time.deltaTime);
            pointPositions[i] = points[i].transform.position;
        }
        lr.SetPositions(pointPositions);
        if (Vector3.Distance(points[step].transform.position, anchors[step + 1].transform.position) <= 0.25f)
        {
            step++;
        }
    }
}
