﻿using UnityEngine;
using System.Collections;

public class RotateObject : MonoBehaviour {
    //public float startingAngle = 0;
    public float rotateSpeed = 3f;
    public enum RotationAxis {X,Y,Z};
    public RotationAxis axisToRotateAround = RotationAxis.Y;
	// Use this for initialization
	void Start ()
    {
        //this.transform.rotation = Quaternion.Euler(new Vector3(0, startingAngle, 0));
	}
	
	// Update is called once per frame
	void Update ()
    {
        switch(axisToRotateAround)
        {
            case RotationAxis.X:
                this.transform.Rotate(new Vector3(rotateSpeed * Time.deltaTime, 0, 0));

                break;
            case RotationAxis.Y:
                this.transform.Rotate(new Vector3(0, rotateSpeed * Time.deltaTime, 0));

                break;
            case RotationAxis.Z:
                this.transform.Rotate(new Vector3(0, 0, rotateSpeed * Time.deltaTime));

                break;

        }
	}
}
