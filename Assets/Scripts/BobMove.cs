﻿using UnityEngine;
using System.Collections;

public class BobMove : MonoBehaviour
{
    public float freq = 1f;
    public float mag = 1f;
	void Start ()
    {
        mag = Random.Range(-0.0001f, 0.0001f);
        freq = Random.Range(0.1f, 1f);

    }

    void Update ()
    {
        transform.Rotate(Vector3.up, Time.deltaTime);
        transform.position = transform.position + Vector3.up * Mathf.Sin(Time.time * freq) * mag;
	}
}
