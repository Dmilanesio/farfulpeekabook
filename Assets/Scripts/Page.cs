﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Page", menuName = "Inventory/List", order = 1)]
public class Page : ScriptableObject
{
    public string objectName = "New Page";
    public int pageNumber;
    public string[] pageTexts;
}
