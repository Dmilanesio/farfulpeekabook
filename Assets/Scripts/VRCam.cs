﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;
public class VRCam : MonoBehaviour
{
    [SerializeField]
    Image fader;

    [SerializeField]
    Image cursor;

    #region Singleton
    private static VRCam instance;
    public static VRCam Instance
    {
        get { return instance; }
    }
    #endregion

    // Use this for initialization
    public void Awake ()
    {
        instance = this;
	}
	
	// Update is called once per frame
	public void FadeIn (float time)
    {
        fader.material.DOFade(0,time);
	}

    public void FadeOut(float time)
    {
        fader.material.DOFade(1, time);
    }

    public void FadeCursor(float alpha)
    {
        cursor.color = new Color(0, 0, 0, alpha);
    }
}