﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class IdleState : State<GameStateData>
{
    public string stateName = "IdleState";
    public override string GetState()
    {
        return stateName;
    }

    public override void EnterState(GameStateData data)
    {

        base.Init();
    }
    public override void Tick(GameStateData data)
    {

    }

    public override void ExitState(GameStateData data)
    {

    }
}

