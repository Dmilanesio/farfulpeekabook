﻿using UnityEngine;
using System.Collections;

public abstract class StateData : MonoBehaviour
{
    public bool isDebug = false;
    public bool isInit = false;

    public virtual void Init()
    {
        isInit = true;
    }

    public virtual void Reset()
    {

    }
}

