﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

public class GameStateData : StateData
{
    
    public StateMachine<GameStateData> stateMachine;
    public State<GameStateData> initialState;

    public static GameStateData Instance;
    


    public override void Init()
    {
       
        base.Init();
    }

    public override void Reset()
    {
        //used to reset stateData
        base.Reset();
    }

   
    void Awake()
    {
        Instance = this;

    }
    void Start()
    {
        Init();
    }
    void Update()
    {
       
    }

}