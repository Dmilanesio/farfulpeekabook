﻿using UnityEngine;
using System.Collections;
using System;

public class StateMachine<T> where T : StateData
{
    public State<T> previousState;
    public State<T> currentState;
    State<T> targetState;
    public T stateData;

    public void Init(State<T> initialState)
    {
        //stateData = (T)Activator.CreateInstance(typeof(T));
        NewState(initialState);
    }

    public void Tick()
    {
        if (currentState.isInit)
        {
            currentState.Tick(stateData);
        }

        //       if(stateData.isDebug)
        //        Debug.Log("Your current state is: " + currentState.GetState());
    }

    public void FixedTick()
    {
        if (currentState.isInit)
        {
            currentState.FixedTick(stateData);
        }
    }

    public void NewState(State<T> newState)
    {
        if (currentState != null)
        {
            if (newState.GetState() == currentState.GetState())
            {
                return;
            }
            currentState.isInit = false;

            previousState = currentState;
            currentState.ExitState(stateData);
        }

        currentState = newState;
        currentState.stateMachine = this;
        currentState.EnterState(stateData);

        if (stateData.isDebug && previousState != null)
        {
            Debug.Log("Your current state is: " + currentState.GetState() + " and your previous state was: " + previousState.GetState());
        }
        else if (stateData.isDebug)
        {
            Debug.LogWarning("Previous state does not exist.");
        }
    }
    public string CheckState()
    {
        return currentState.ToString();
    }
}

