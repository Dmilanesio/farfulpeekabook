﻿using UnityEngine;
using System.Collections;

public abstract class State<T> where T : StateData
{

    public bool isInit = false;

    public StateMachine<T> stateMachine;

    public void Init()
    {
        isInit = true;
    }

    public virtual string GetState()
    {
        return null;
    }

    public virtual void EnterState(T data)
    {
    }

    public virtual void Tick(T data)
    {

    }

    public virtual void FixedTick(T data)
    {

    }

    public virtual void ExitState(T data)
    {

    }
}

