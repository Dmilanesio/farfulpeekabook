﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class LookAtObject : MonoBehaviour {
    public float introDot = 0;
    float lookAtCounter = 0;
    public UnityEvent select;
    public Animator anim;
    public bool isLookAt;
    public bool invokeOnLookAt;
    public float gazeLimitTimer = 0f;
    public float manualGazeRange = 0f;
    public bool useManualValue;
    void Start ()
    {

	}
	
	void Update ()
    {
        introDot = Vector3.Dot(BookController.Instance.headCam.transform.forward.normalized, (transform.position - BookController.Instance.headCam.transform.position).normalized);
        gazeLimitTimer -= Time.deltaTime;
        if(!useManualValue)
        {
            if (introDot >= 0.992f && !BookController.Instance.isTransitioning)
            {
                if (!invokeOnLookAt)
                {
                    IsLookAt(true);
                    isLookAt = true;
                }
                else
                {
                    if (gazeLimitTimer <= 0)
                    {
                        Select();
                    }
                    gazeLimitTimer = 2f;
                }
            }
            else
            {
                if (!invokeOnLookAt)
                {
                    IsLookAt(false);
                    isLookAt = false;
                }
                else
                {

                }
            }
        }
        else
        {
            if (introDot >= manualGazeRange && !BookController.Instance.isTransitioning)
            {
                if (!invokeOnLookAt)
                {
                    IsLookAt(true);
                    isLookAt = true;
                }
                else
                {
                    if (gazeLimitTimer <= 0)
                    {
                        Select();
                    }
                    gazeLimitTimer = 2f;
                }
            }
            else
            {
                if (!invokeOnLookAt)
                {
                    IsLookAt(false);
                    isLookAt = false;
                }
                else
                {

                }
            }
        }
    }

    public bool IsLookAt(bool isLook)
    {
        if(isLook)
        {
            if(anim != null)
            {
                anim.SetBool("isTargeting", true);

            }

            lookAtCounter += Time.deltaTime;
            if (lookAtCounter >= 1.6f)
            {
                Select();
                lookAtCounter = 0;
                if (anim != null)
                {
                    anim.SetBool("isTargeting", false);
                }
                return true;
            }
        }
        
        else
        {
            if (anim != null)
            {
                anim.SetBool("isTargeting", false);
            }
            lookAtCounter = 0;
            return false;
        }
        return false;
    }
    public void Select()
    {
        select.Invoke();
    }
}
