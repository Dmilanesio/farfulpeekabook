﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class BookController : MonoBehaviour
{
    public GameObject headCam;
    public static BookController Instance;
    public int currentPageNumber;
    public int totalPages;
    PageController currentPage;
    public CanvasGroup fadeGroup;
    public CanvasGroup blueFadeGroup;
    public bool isTransitioning = false;
    public enum FadeType { FADETOBLACK, GOO, BLUE}
    private FadeType currentFadeSetting = FadeType.GOO;
    private FadeType previousFadeSetting = FadeType.GOO;
    public MeshRenderer gooShell;
    public MeshRenderer oozeShell;
    Material gooMaterial;
    Material oozeMaterial;
    public Animator reticle;
    bool isLooking;
    public LookAtObject[] consoleButtons;
    public Animator shipAnimator;
    private Vector3 startPosition;
    public GameObject starAimer;
    public Text starAimerText;
    float timeBeforeAimerShown = 3f;
    public CanvasGroup preReticle;
    public LookAtObject previousPage;
    public LookAtObject nextPage;
    // Use this for initialization
    void Awake()
    {
        startPosition = shipAnimator.transform.position;
        Instance = this;
        DontDestroyOnLoad(this.gameObject);

    }
    void Start()
    {
        currentPageNumber = 0;
        if(PageController.Instance != null)
        {
            currentPage = PageController.Instance;
        }
        totalPages = SceneManager.sceneCountInBuildSettings;
        NextPage();
        SceneManager.sceneLoaded += StartLevel;
        gooMaterial = gooShell.material;
        oozeMaterial = oozeShell.material;

    }

    // Update is called once per frame
    void Update()
    {
        if (!isTransitioning && currentPageNumber > 0)
        {
            if(!currentPage.pageButtons[currentPage.starProgress].isDeactivated)
            {
                CheckTarget();
                timeBeforeAimerShown -= Time.deltaTime;
                if (timeBeforeAimerShown <= 0 && !starAimer.activeInHierarchy)
                {
                    ShowAimer(true);
                }
            }
            
        }
        else
        {
            preReticle.alpha = 0;
        }
        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            JumpPage(1);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            JumpPage(2);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            JumpPage(3);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            JumpPage(4);
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            JumpPage(5);
        }
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            JumpPage(6);
        }
        if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            JumpPage(7);
        }
        if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            JumpPage(8);
        }
        if (Input.GetKeyDown(KeyCode.Alpha9))
        {
            JumpPage(9);
        }
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            JumpPage(10);
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            JumpPage(11);
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            JumpPage(12);
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            JumpPage(13);
        }

    }
    public void JumpPage(int page)
    {
        isTransitioning = true;
        StartCoroutine(StartTransition(page, currentFadeSetting));

    }
    public void NextPage()
    {
        if(SceneManager.GetActiveScene().buildIndex != 0)
        {
            currentFadeSetting = currentPage.pageExitFade;
        }

        isTransitioning = true;
        currentPageNumber++;
        if (currentPageNumber >= totalPages)
        {
            StartCoroutine(StartTransition(1, currentFadeSetting));
            currentPageNumber = 1;
        }
        else
        {
            StartCoroutine(StartTransition(currentPageNumber, currentFadeSetting));
        }
    }
    public void PreviousPage()
    {
        if (SceneManager.GetActiveScene().buildIndex != 0)
        {
            currentFadeSetting = currentPage.pageExitFade;
        }

        isTransitioning = true;
        currentPageNumber--;
        if (currentPageNumber <= 0)
        {
            StartCoroutine(StartTransition(totalPages - 1, currentFadeSetting));
            currentPageNumber = totalPages - 1;
        }
        else
        {
            StartCoroutine(StartTransition(currentPageNumber, currentFadeSetting));
        }
    }
    public IEnumerator StartTransition(int target , FadeType type)
    {
        previousFadeSetting = currentFadeSetting;
        yield return new WaitForSeconds(0.25f);
        switch(type)
        {
            case FadeType.FADETOBLACK:
                fadeGroup.DOFade(1, 1f);
                break;
            case FadeType.GOO:
                fadeGroup.alpha = 0;
                gooMaterial.DOOffset(new Vector2(0f, 0f), 1f);
                oozeMaterial.DOOffset(new Vector2(0f, 0f), 1f);
            
                //gooMaterial.SetTextureOffset("_MainTex", new Vector2(0.5f, 0f));
                break;
            case FadeType.BLUE:
                blueFadeGroup.DOFade(1, 1f);
                break;
        }
        yield return new WaitForSeconds(1.25f);
        if(currentFadeSetting != FadeType.BLUE)
        {
            fadeGroup.DOFade(1, 0.2f);
        }
        yield return new WaitForSeconds(0.25f);

        shipAnimator.enabled = false;
        shipAnimator.transform.position = startPosition;
        SceneManager.LoadScene(target);
    }
    public IEnumerator EndTransition()
    {
        yield return new WaitForSeconds(1f);
        if (currentFadeSetting != FadeType.BLUE)
        {
            fadeGroup.DOFade(0, 0.25f);
        }
        switch (previousFadeSetting)
        {
            case FadeType.FADETOBLACK:
                fadeGroup.alpha = 1;
                fadeGroup.DOFade(0, 1f);
                break;
            case FadeType.GOO:
                gooMaterial.DOOffset(new Vector2(0f, 0.5f), 2f);
                oozeMaterial.DOOffset(new Vector2(0f, 0.5f), 2.65f);
                //gooMaterial.SetTextureOffset("_MainTex", new Vector2(0.5f, 0.5f));
                break;
            case FadeType.BLUE:
                blueFadeGroup.alpha = 1;
                blueFadeGroup.DOFade(0, 1f);
                break;
        }
        reticle.SetBool("isTargeting", false);
        yield return new WaitForSeconds(0.25f);
        isTransitioning = false;
    }
    public void StartLevel(Scene scene, LoadSceneMode mode)
    {
        StartCoroutine(EndTransition());
        currentPage = PageController.Instance;
        currentPage.Init();
    }
    public void CheckTarget()
    {
        starAimer.transform.LookAt(currentPage.pageButtons[currentPage.starProgress].transform.position);
        float introDot = Vector3.Dot(headCam.transform.forward.normalized, (currentPage.pageButtons[currentPage.starProgress].transform.position - headCam.transform.position).normalized);
        VRCam.Instance.FadeCursor(Mathf.Pow(introDot, 20));
        //star.spawnedCircle.transform.localScale = new Vector3((1 - introDot) * 5f, (1 - introDot) * 5f, 1f) + star.transform.localScale.normalized;
        bool buttonsCheck = false;
        //for (int i = 0; i < consoleButtons.Length; i++)
        //{
        //    if (consoleButtons[i].isLookAt)
        //    {
        //        buttonsCheck = true;
        //    }
        //}
        preReticle.alpha = Mathf.InverseLerp(0.9f, 1f, introDot);
        if (introDot >= 0.992f)
        {
            ShowAimer(false);
            currentPage.pageButtons[currentPage.starProgress].IsLookAt(true);
            buttonsCheck = true;
        }
        else
        {
            currentPage.pageButtons[currentPage.starProgress].IsLookAt(false);
            if (nextPage.introDot >= 0.992f)
            {
                buttonsCheck = true;
                VRCam.Instance.FadeCursor(Mathf.Pow(nextPage.introDot, 20));
                preReticle.alpha = Mathf.InverseLerp(0.9f, 1f, nextPage.introDot);

            }
            if (previousPage.introDot >= 0.992f)
            {
                buttonsCheck = true;
                VRCam.Instance.FadeCursor(Mathf.Pow(previousPage.introDot, 20));
                preReticle.alpha = Mathf.InverseLerp(0.9f, 1f, previousPage.introDot);

            }
        }
        
        if (buttonsCheck)
        {
            reticle.SetBool("isTargeting", true);

        }
        else
        {
            reticle.SetBool("isTargeting", false);

        }
    }
    public void ShowAimer(bool isOn)
    {
        if(isOn)
        {
            starAimer.SetActive(true);
            starAimerText.DOFade(1, 0.5f);
        }
        else
        {
            starAimer.SetActive(false);
            starAimerText.DOFade(0, 0.5f);
            timeBeforeAimerShown = 3f;
        }
    }
}
