﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class AutomaticStepSequencer : MonoBehaviour
{
    public float[] waitTimes;
    float timer;
    public bool storyStar;
    public Text storyStarLabel;
	void Start ()
    {
        timer = waitTimes[0];
        PageController.Instance.StepAction += ResetTimer;
	}
	
	void Update ()
    {
        if(PageController.Instance.starProgress < PageController.Instance.pageButtons.Length - 1)
        {
            if (storyStar)
            {
                if (PageController.Instance.starProgress > 0)
                {
                    timer -= Time.deltaTime;
                    if (timer <= 0)
                    {
                        storyStarLabel.DOFade(0, 0.25f);
                        PageController.Instance.pageButtons[PageController.Instance.starProgress].Select();
                        //PageController.Instance.Step();
                    }
                }
            }
            else
            {
                timer -= Time.deltaTime;
                if (timer <= 0)
                {
                    PageController.Instance.pageButtons[PageController.Instance.starProgress].Select();
                    //PageController.Instance.Step();
                }
            }
        }
	}
    public void ResetTimer()
    {
        if(PageController.Instance.starProgress >= waitTimes.Length)
        {
            Debug.Log("finished sequence");
        }
        else
        {
            timer = waitTimes[PageController.Instance.starProgress];
        }
    }
}
