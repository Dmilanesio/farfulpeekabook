﻿using UnityEngine;
using System.Collections;

public class FarfulControl : MonoBehaviour
{
    public SpriteRenderer farfulSprite;
    public Animator farfulAnimator;
    public GameObject farfulObj;
    public Animator spriteAnimator;
	void Start ()
    {
        farfulObj = farfulSprite.gameObject;
	}
	
	void Update ()
    {

	}
    public void BeginAnimation()
    {
        farfulSprite.enabled = true;
        farfulAnimator.enabled = true;
        spriteAnimator.SetBool("isStarted", true);
    }
}
