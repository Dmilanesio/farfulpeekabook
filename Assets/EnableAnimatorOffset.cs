﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableAnimatorOffset : MonoBehaviour
{
    Animator anim;
    public float offset;
    public TrailRenderer trail;
	void Start ()
    {
        anim = GetComponent<Animator>();
	}
	
	void Update ()
    {
        offset -= Time.deltaTime;
        if(offset <= 0)
        {
            anim.enabled = true;
        }
	}
    public void DisableTrail()
    {
        trail.enabled = false;
    }
    public void EnableTrail()
    {
        trail.enabled = true;
    }
}
