// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Saturator"
{
	Properties
	{
		[HideInInspector] __dirty( "", Int ) = 1
		_Desaturation("Desaturation", 2D) = "white" {}
		_Diffuse("Diffuse", 2D) = "white" {}
		_DesaturateAmount("DesaturateAmount", Float) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _Diffuse;
		uniform float4 _Diffuse_ST;
		uniform float _DesaturateAmount;
		uniform sampler2D _Desaturation;
		uniform float4 _Desaturation_ST;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_Diffuse = i.uv_texcoord * _Diffuse_ST.xy + _Diffuse_ST.zw;
			float2 uv_Desaturation = i.uv_texcoord * _Desaturation_ST.xy + _Desaturation_ST.zw;
			float temp_output_8_0 = ( _DesaturateAmount + tex2D( _Desaturation, uv_Desaturation ).a );
			float3 desaturateVar1 = lerp( tex2D( _Diffuse, uv_Diffuse ).xyz,dot(tex2D( _Diffuse, uv_Diffuse ).xyz,float3(0.299,0.587,0.114)),temp_output_8_0);
			o.Albedo = desaturateVar1;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=11004
3267;29;566;1004;1881.8;916.0993;1.6;False;False
Node;AmplifyShaderEditor.SamplerNode;3;-1273.698,-112.5;Float;True;Property;_Desaturation;Desaturation;0;0;Assets/Textures/Desaturator.png;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;6;-1205.399,-205.1;Float;False;Property;_DesaturateAmount;DesaturateAmount;2;0;1;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.SamplerNode;5;-869.6003,-402.4998;Float;True;Property;_Diffuse;Diffuse;1;0;Assets/Textures/RainbowGrade.png;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;8;-846.5997,-124.4999;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.NoiseGeneratorNode;9;-920.6998,175.8004;Float;False;Simplex2D;1;0;FLOAT2;0,0;False;1;FLOAT
Node;AmplifyShaderEditor.SinTimeNode;10;-1183.299,222.6002;Float;False;0;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleSubtractOpNode;11;-660.6995,18.50028;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.DesaturateOpNode;1;-364.9001,-210.2;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;0.0;False;1;FLOAT3
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;0,0;Float;False;True;2;Float;ASEMaterialInspector;0;Standard;Saturator;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;Opaque;0.5;True;True;0;False;Opaque;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;False;0;4;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;Add;Add;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;Relative;0;;-1;-1;-1;-1;0;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;OBJECT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;8;0;6;0
WireConnection;8;1;3;4
WireConnection;9;0;10;2
WireConnection;11;0;8;0
WireConnection;11;1;9;0
WireConnection;1;0;5;0
WireConnection;1;1;8;0
WireConnection;0;0;1;0
ASEEND*/
//CHKSM=8A42B0326D48C8BAC06212D7DFFF0FBDCF1A30B9