﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableRendererOnStart : MonoBehaviour
{
    void Start()
    {
        this.GetComponent<MeshRenderer>().enabled = true;
    }
}