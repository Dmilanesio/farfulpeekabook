﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using System;
public class PageController : MonoBehaviour
{
    public static PageController Instance;
    public StarButton[] pageButtons;
    public int starProgress = 0;
    public BookController.FadeType pageExitFade;
    public TextMeshPro[] initialText;
    public GameObject debugZones;
    private AudioSource source;
    public Action StepAction = delegate { };
    public Action InitAction = delegate { };
    bool transitioning = false;
    public float holdTime = 0;
    bool firstStarActivated = false;
    void Awake()
    {
        Instance = this;
        source = this.GetComponent<AudioSource>();
    }
    void Start()
    {
        for (int i = 0; i < initialText.Length; i++)
        {
            initialText[i].DOFade(1, 0.5f);
        }
    }
    private void Update()
    {
        holdTime -= Time.deltaTime;
        if(holdTime <=0 && !firstStarActivated)
        {
            pageButtons[0].Activate();
            firstStarActivated = true;
        }

    }
    public void Step()
    {
        StepAction();
        starProgress++;
        if(starProgress == pageButtons.Length)
        {
            BookController.Instance.NextPage();
        }
        
    }
    
    public void ActivateNextStar()
    {
        pageButtons[starProgress].Activate();
    }
    public void Init()
    {
        for (int i = 0; i < pageButtons.Length; i++)
        {
            pageButtons[i].Init();
            debugZones.SetActive(false);
        }
        InitAction();
    }
    public void PlayNarration(AudioClip clip)
    {
        source.clip = clip;
        source.Play();
    }
    public void AnimationStep(int stepAnimNum)
    {
        BookController.Instance.shipAnimator.SetInteger("StepProgress", stepAnimNum);
    }
}
