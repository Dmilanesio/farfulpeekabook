﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour
{
    private Camera cam;
    private Vector3 up;
    void Start()
    {
        up = this.transform.up;
        if (Camera.main != null)
        {
            cam = Camera.main;
        }
        else
        {
            cam = GameObject.Find("TestCamera").GetComponent<Camera>();
        }
    }
	void Update ()
    {
        this.transform.rotation = Quaternion.LookRotation(this.transform.position - cam.transform.position, up);
    }
}
