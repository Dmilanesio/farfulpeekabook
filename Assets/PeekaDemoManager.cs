﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class PeekaDemoManager : MonoBehaviour
{
    public static PeekaDemoManager instance;
    public enum GameState { START, WELCOME, FARAWAY, TOTHEPLANET, THELANDWHERE, EARTHZOOM, END, RESTART}
    int step = 0;
    public GameState gameState = GameState.START;
    public Animator consoleAnimator;
    public GameObject rotater;
    public Animator[] textAnimators;
    public StarButton[] triggerStars;
    public Camera headCam;
    public GuideSpawner spawner;
    public AudioClip[] clips;
    public AudioSource source;
    public StarButton[] tutorialStars;
    //Start
    public CanvasGroup fadeGroup;
    float startTimer = 2f;
    public CanvasGroup leftEyeRet;
    public CanvasGroup rightEyeRet;

    //Welcome
    public MeshRenderer sphereObj;
    private Material sphereToFade;
    private float welcomeTimer = 6f;
    public Animator welcomeText;
    public CanvasGroup peekaGroup;
    bool peekaFaded;
    bool tutorialComplete;
    float tutorialPauseTimer = 2f;
    //public ParticleSystem warpParticles;
    float fadeOut = 1500f;
    //Faraway
    public CanvasGroup tutorialCanvas;
    float inputWaitTimer = 8f;
    //Totheplanet
    public FarfulControl farful;
    public Animator colorTextAnimator;
    //Thelandwhere
    bool audioStarted;
    //EarthZoom
    public CanvasGroup restartGroup;
    float endTimer = 5f;
    public ParticleSystem warpParticles;
    bool warping;
    public MeshRenderer warpTube;
    private Material warpTubeMat;
    //End
	void Start ()
    {
        instance = this;
        sphereToFade = sphereObj.material;
        warpTubeMat = warpTube.material;
	}
	
	void Update ()
    {
        farful.farfulObj.transform.rotation = Quaternion.LookRotation(farful.farfulObj.transform.position - headCam.transform.position);
        switch(gameState)
        {
            case GameState.START:
                startTimer -= Time.deltaTime;
                if(startTimer <= 0)
                {
                    VRCam.Instance.FadeIn(1f);
                    //fadeGroup.DOFade(0, 1f);
                    welcomeText.SetTrigger("Start");
                    gameState = GameState.WELCOME;
                    
                }
                break;
            case GameState.WELCOME:
               

                rotater.transform.parent = null;
                welcomeTimer -= Time.deltaTime;
                if(welcomeTimer <= 3f && !peekaFaded)
                {
                    FadeUPPeeka();
                    foreach (StarButton star in tutorialStars)
                    {
                        star.Activate();
                    }
                    tutorialCanvas.DOFade(1, 2f);
                    
                    peekaFaded = true;
                }
                if(welcomeTimer <= 0 && tutorialPauseTimer <= 0)
                {
                    FadeSphereOut();
                    step++;
                    consoleAnimator.SetInteger("Step", step);
                    peekaGroup.DOFade(0, 1.5f);
                    tutorialCanvas.DOFade(0, 1f);

                    spawner.canSpawn = true;
                    gameState = GameState.FARAWAY;
                }
                int completeStars = 0;
                foreach(StarButton star in tutorialStars)
                {
                    float introDot = Vector3.Dot(headCam.transform.forward.normalized, (star.transform.position - headCam.transform.position).normalized);
                    VRCam.Instance.FadeCursor(Mathf.Pow(introDot, 20));
                    //star.spawnedCircle.transform.localScale = new Vector3((1 - introDot) * 5f, (1 - introDot) * 5f, 1f) + star.transform.localScale.normalized;
                    if (introDot >= 0.99f && welcomeTimer <= -0.5f)
                    {
                        star.Select();
                    }
                    if (star.triggered)
                    {
                        completeStars++;
                        
                        star.starAnimator.SetTrigger("Select");
                    }
                }
                if(completeStars == 3)
                {
                    tutorialComplete = true;
                }
                if(tutorialComplete)
                {
                    tutorialPauseTimer -= Time.deltaTime;
                }
                break;
            case GameState.FARAWAY:
                fadeOut -= 500 * Time.deltaTime;
                //ParticleSystem.EmissionModule emit = warpParticles.emission;
                //emit.rate = new ParticleSystem.MinMaxCurve(0, fadeOut);

                //rotater.transform.rotation = Quaternion.Slerp(rotater.transform.rotation, Quaternion.LookRotation(rotater.transform.forward, Vector3.down), Time.deltaTime * 0.3f);
                float dot = Vector3.Dot(headCam.transform.forward.normalized, (triggerStars[0].transform.position - headCam.transform.position).normalized);
                //triggerStars[0].spawnedCircle.transform.localScale = new Vector3((1 - dot) * 5f, (1 - dot) * 5f, 1f);

                if (dot > 0)
                {
                    rightEyeRet.alpha = Mathf.Pow(dot, 20);
                    leftEyeRet.alpha = Mathf.Pow(dot, 20);
                    VRCam.Instance.FadeCursor(Mathf.Pow(dot, 20));
                }
                

                inputWaitTimer -= Time.deltaTime;
                if(inputWaitTimer <= 0)
                {
                    if (dot > 0.995f)
                    {
                        
                        rotater.gameObject.SetActive(false);
                        source.clip = clips[0];
                        source.Play();
                        triggerStars[0].starAnimator.SetTrigger("Select");
                        triggerStars[0].Select();
                        StartCoroutine(MoveObject(triggerStars[0].gameObject, triggerStars[1].transform.position, 2f));
                        step++;
                        consoleAnimator.SetInteger("Step", step);
                        inputWaitTimer = 4f;
                        gameState = GameState.TOTHEPLANET;
                    }
                }
                break;

            case GameState.TOTHEPLANET:
                float dot1 = Vector3.Dot(headCam.transform.forward.normalized, (triggerStars[1].transform.position - headCam.transform.position).normalized);
                //triggerStars[1].spawnedCircle.transform.localScale = new Vector3((1 - dot1) * 5f, (1 - dot1) * 5f, 1f);

                inputWaitTimer -= Time.deltaTime;
                if (dot1 > 0)
                {
                    rightEyeRet.alpha = Mathf.Pow(dot1, 20);
                    leftEyeRet.alpha = Mathf.Pow(dot1, 20);
                    VRCam.Instance.FadeCursor(Mathf.Pow(dot1, 20));
                }
                if (inputWaitTimer <= 0)
                {
                    if (dot1 > 0.995f)
                    {
                        source.clip = clips[1];
                        source.Play();
                        triggerStars[1].starAnimator.SetTrigger("Select");
                        triggerStars[1].Select();
                        //triggerStars[0].Deactivate();
                        textAnimators[0].GetComponent<SpriteRenderer>().DOFade(0, 1f);
                        StartCoroutine(MoveObject(triggerStars[1].gameObject, triggerStars[2].transform.position, 4f));

                        step++;
                        consoleAnimator.SetInteger("Step", step);
                        inputWaitTimer = 10f;
                        StartCoroutine(StartFarful());
                        gameState = GameState.THELANDWHERE;
                    }
                }
                break;
            case GameState.THELANDWHERE:
                float dot2 = Vector3.Dot(headCam.transform.forward.normalized, (triggerStars[2].transform.position - headCam.transform.position).normalized);
                //triggerStars[2].spawnedCircle.transform.localScale = new Vector3((1 - dot2) * 5f, (1 - dot2) * 5f, 1f);

                inputWaitTimer -= Time.deltaTime;
                if (dot2 > 0)
                {
                    rightEyeRet.alpha = Mathf.Pow(dot2, 20);
                    leftEyeRet.alpha = Mathf.Pow(dot2, 20);
                    VRCam.Instance.FadeCursor(Mathf.Pow(dot2, 20));

                }
                if (inputWaitTimer <= 0)
                {
                    if (dot2 > 0.995f && !audioStarted)
                    {
                        source.clip = clips[2];
                        source.Play();
                        triggerStars[1].Deactivate();
                        textAnimators[1].GetComponent<SpriteRenderer>().DOFade(0, 1f);
                        triggerStars[2].Select();
                        triggerStars[2].starAnimator.SetTrigger("Select");
                        
                        
                        audioStarted = true;
                    }
                }
                if(inputWaitTimer <= -6f)
                {
                    step++;
                    consoleAnimator.SetInteger("Step", step);
                    inputWaitTimer = 4f;
                    gameState = GameState.EARTHZOOM;
                }
                break;

            case GameState.EARTHZOOM:
                endTimer -= Time.deltaTime;
                if(endTimer <= 2f && !warping)
                {
                    warpParticles.Play();
                    warpTubeMat.DOFade(1, 3f);

                    warping = true;
                }
                if (endTimer <= -1f)
                {
                    FadeUIUp();

                    farful.farfulSprite.DOFade(0, 1f);
                    textAnimators[2].GetComponent<SpriteRenderer>().DOFade(0, 1f);

                    triggerStars[2].Deactivate();
                    restartGroup.DOFade(1, 0.2f);
                    

                    triggerStars[3].Activate();
                    gameState = GameState.END;
                }
                break;

            case GameState.END:
                float dot3 = Vector3.Dot(headCam.transform.forward.normalized, (triggerStars[3].transform.position - headCam.transform.position).normalized);
                //triggerStars[3].spawnedCircle.transform.localScale = new Vector3((1 - dot3) * 5f, (1 - dot3) * 5f, 1f);
                VRCam.Instance.FadeCursor(Mathf.Pow(dot3, 20));

                if (dot3 > 0)
                {
                    rightEyeRet.alpha = Mathf.Pow(dot3, 20);
                    leftEyeRet.alpha = Mathf.Pow(dot3, 20);
                    VRCam.Instance.FadeCursor(Mathf.Pow(dot3, 20));
                }
                if (dot3 > 0.995f)
                {
                    triggerStars[3].starAnimator.SetTrigger("Select");
                    triggerStars[3].Select();
                    //step++;
                    restartGroup.DOFade(0, 1f);
                    //consoleAnimator.SetInteger("Step", step);
                    inputWaitTimer = 3f;
                    gameState = GameState.RESTART;
                }
                break;

            case GameState.RESTART:
                {
                    inputWaitTimer -= Time.deltaTime;
                    if(inputWaitTimer <= 0)
                    {
                        SceneManager.LoadScene(0);
                    }
                }
                break;
        }
	}
    void FadeSphereOut()
    {
        sphereToFade.DOFade(0, 8f);
    }
    void FadeSphereUp()
    {
        sphereToFade.DOFade(1, 2f);
    }
    void FadeUIOut()
    {
        VRCam.Instance.FadeIn(0.2f);
        //fadeGroup.DOFade(0, 0.2f);
    }
    void FadeUIUp()
    {
        VRCam.Instance.FadeOut(1.5f);
        //fadeGroup.DOFade(1, 1.5f);
    }
    void FadeUPPeeka()
    {
        peekaGroup.DOFade(1, 1f);
    }
    public void TextTrigger(int trigger)
    {
        Debug.Log("Text triggered");
        textAnimators[trigger].SetTrigger("Start");
        triggerStars[trigger].Activate();
        textAnimators[trigger].GetComponent<SpriteRenderer>().color = new Color(1,1,1,1);
        
        if(trigger == 1)
        {
            colorTextAnimator.SetTrigger("Start");
            triggerStars[0].Deactivate();
        }
        if(trigger == 2)
        {
            triggerStars[1].Deactivate();
        }
    }
    IEnumerator StartFarful()
    {
        yield return new WaitForSeconds(4f);
        farful.BeginAnimation();
    }
    IEnumerator MoveObject(GameObject obj, Vector3 targetPosition, float speed)
    {
        yield return new WaitForSeconds(1f);
        obj.transform.DOMove(targetPosition, speed);
    }
}
