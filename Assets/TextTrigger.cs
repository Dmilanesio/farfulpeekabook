﻿using UnityEngine;
using System.Collections;

public class TextTrigger : MonoBehaviour {
    public int triggerNumber = 0;
	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Player")
        {
            PeekaDemoManager.instance.TextTrigger(triggerNumber);
        }
    }
}
