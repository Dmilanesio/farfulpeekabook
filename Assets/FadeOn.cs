﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
public class FadeOn : MonoBehaviour
{
    TextMeshPro text;
    Color textColor;
    void Start()
    {
        text = GetComponent<TextMeshPro>();
        textColor = text.color;
    }
    void Update()
    {
        text.color = textColor;
    }
	public void FadeTextOn()
    {
        text.DOFade(1, 0.25f);
    }

    public void FadeTextOff()
    {
        text.DOFade(0, 0.25f);
    }
}
