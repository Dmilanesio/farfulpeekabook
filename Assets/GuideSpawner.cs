﻿using UnityEngine;
using System.Collections;

public class GuideSpawner : MonoBehaviour
{
    public GameObject guideToSpawn;
    float spawnFreq = 1f;
    float spawnTimer = 1f;
    public GameObject control;
    public bool canSpawn;
	void Start ()
    {
	
	}
	
	void Update ()
    {
        spawnTimer -= Time.deltaTime;
        if(spawnTimer <= 0 && canSpawn)
        {
            Spawn();
        }
	}
    public void Spawn()
    {
        Instantiate(guideToSpawn);
        spawnTimer = spawnFreq;
    }
}
