﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeightAdjustment : MonoBehaviour
{
    public enum HeightAdjustmentSetting { UP, DOWN, RESET}
    public HeightAdjustmentSetting heightSetting = HeightAdjustmentSetting.RESET;
    Vector3 startPos = Vector3.zero;
    float incrementValue = 0.25f;
    int stage = 0;
    public GameObject cameraToAdjust;
    void Start()
    {
        startPos = cameraToAdjust.transform.position;
    }
    public void ResetPosition()
    {
        cameraToAdjust.transform.position = startPos;
    }
    public void Raise()
    {
        if(stage<5 && stage > -5)
        {
            cameraToAdjust.transform.position += new Vector3(0, incrementValue, 0);
            stage++;
        }
    }
    public void Lower()
    {
        if (stage < 5 && stage > -5)
        {
            cameraToAdjust.transform.position -= new Vector3(0, incrementValue, 0);
            stage--;
        }
    }

}
