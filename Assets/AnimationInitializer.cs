﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class AnimationInitializer : MonoBehaviour
{
    public RuntimeAnimatorController pageAnimator;
	void Awake ()
    {
        this.GetComponent<PageController>().InitAction += StepAnimation;
	}
    void StepAnimation()
    {
        BookController.Instance.shipAnimator.enabled = true;
        BookController.Instance.shipAnimator.runtimeAnimatorController = pageAnimator;
        BookController.Instance.shipAnimator.SetInteger("StepProgress", 1);
    }
    void ResetShip()
    {
        BookController.Instance.shipAnimator.enabled = false;
        BookController.Instance.shipAnimator.SetInteger("StepProgress", 0);
    }
}
