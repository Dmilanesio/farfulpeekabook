﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleRingFollower : MonoBehaviour
{
    ParticleSystem particles;
    public float enabledTime = 7f;
    bool isEnabled;
    private void Start()
    {
        particles = this.GetComponent<ParticleSystem>();
    }
    void Update ()
    {
        transform.position = BookController.Instance.shipAnimator.gameObject.transform.position + new Vector3(0, 3f, 15f);
        if(isEnabled)
        {
            enabledTime -= Time.deltaTime;
            if(enabledTime <=0)
            {
                StopParticles();
                isEnabled = false;
            }
        }
	}
    public void StartParticles()
    {
        particles.Play();
        isEnabled = true;
    }
    public void StopParticles()
    {
        particles.Stop();
    }
}
