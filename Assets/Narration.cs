﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Narration : MonoBehaviour
{
    public AudioClip narration;
    void Start()
    {
        this.GetComponent<StarButton>().SelectAction += PlayNarration;
    }
    void PlayNarration()
    {
        PageController.Instance.PlayNarration(narration);
    }
}
