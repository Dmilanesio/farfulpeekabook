﻿using UnityEngine;
using System.Collections;

public class LookMatcher : MonoBehaviour {

    public Rigidbody rbToFace;
    Vector3 currentPos;
    Vector3 lastPos;
    Vector3 posDiff;
	// Use this for initialization
	void Start ()
    {
        currentPos = rbToFace.transform.position;
        lastPos = rbToFace.transform.position;
        posDiff = currentPos;
	}
	
	// Update is called once per frame
	void Update ()
    {
        
        if(currentPos != lastPos)
        {
            this.transform.forward = Vector3.Lerp(this.transform.forward, posDiff, 50 * Time.deltaTime);
            this.transform.rotation = Quaternion.LookRotation(this.transform.forward, Vector3.up);
        }
        //this.transform.position = rbToFace.position;
    }
    void FixedUpdate()
    {
        lastPos = currentPos;
        currentPos = rbToFace.transform.position;
        posDiff = currentPos - lastPos;
    }
}
