﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using DG.Tweening;
using TMPro;
using System;
public class StarButton : MonoBehaviour
{

    public ParticleSystem magicParticles;
    public GameObject[] trails;
    public Rigidbody starRigidbody;
    public Animator starAnimator;
    public MeshRenderer mr;
    private Material mrMat;
    public bool triggered;
    [HideInInspector]
    public Camera playerCam;
    public UnityEvent StepPage;
    public TextMeshPro[] textToFade;
    public Action SelectAction = delegate { };
    public float fadeUpDelay = 0;
    public float fadeDownDelay = 0;
    public bool fadeDownOnNextStar = false;
    private AudioSource sparkle;
    bool isInit;
    private float lookAtCounter = 0;
    public float waitTimer = 0f;
    bool isTransitioning = false;
    public bool isDeactivated = true;
    public bool fadeTextOnSelect;
    private Vector3 startScale;
    public float starSpeed = 3f;
    public void Start()
    {

        startScale = this.gameObject.transform.localScale;
        sparkle = this.gameObject.AddComponent<AudioSource>();
        sparkle.clip = Resources.Load<AudioClip>("Sparkle");
        sparkle.pitch = UnityEngine.Random.Range(1f, 2f);
    }
    public void Init()
    {
        playerCam = Camera.main;
        PageController.Instance.gameObject.GetComponentInChildren<Camera>().enabled = false;

        sparkle = this.gameObject.AddComponent<AudioSource>();
        sparkle.clip = Resources.Load<AudioClip>("Sparkle");
        sparkle.pitch = UnityEngine.Random.Range(1f, 2f);

        starAnimator.gameObject.SetActive(false);
        if (mr != null)
        {
            mrMat = mr.material;
        }
        for (int i = 0; i < trails.Length; i++)
        {
            AnimationCurve curve = AnimationCurve.EaseInOut(0f, .02f, 1f, 0f);
            trails[i].GetComponent<TrailRenderer>().widthCurve = curve;
        }
        isInit = true;
    }
	void Update ()
    {
        if(isInit)
        {
            this.transform.LookAt(playerCam.transform.position);
            if (PageController.Instance.starProgress < PageController.Instance.pageButtons.Length)
            {
                if (Vector3.Distance(transform.position, PageController.Instance.pageButtons[PageController.Instance.starProgress].transform.position) < 0.01f && triggered)
                {
                    if(!isDeactivated)
                    {
                        Deactivate();
                    }
                }
            }
        }
        if(isTransitioning)
        {
            waitTimer -= Time.deltaTime;
            if(waitTimer <= 0)
            {
                Transition();
                isTransitioning = false;
            }
        }
	}

    public void Activate()
    {
        this.gameObject.transform.localScale = Vector3.zero;
        this.gameObject.transform.DOScale(startScale, 0.2f);
        magicParticles.Play();
        starAnimator.gameObject.SetActive(true);
        isDeactivated = false;
    }
    public void Deactivate()
    {
        PageController.Instance.ActivateNextStar();
        starAnimator.gameObject.SetActive(false);
        isDeactivated = true;
    }
    public bool IsLookAt(bool isLook)
    {
        if(isLook && !triggered)
        {
            lookAtCounter += Time.deltaTime;
            starAnimator.SetBool("isLookingAt", true);
            if(lookAtCounter >= 1.1f)
            {
                Select();
            }
            return true;
        }
        else
        {
            starAnimator.SetBool("isLookingAt", false);
            lookAtCounter = 0;
            return false;
        }
    }
    public void Select()
    {
        isTransitioning = true;
        starAnimator.SetTrigger("Select");
        if(fadeTextOnSelect)
        {
            FadeTextUp();
        }
    }
    public void FadeTextUp()
    {
        StartCoroutine(FadeUpText());
    }
    public void FadeTextDown()
    {
        Debug.Log("Fading text down");
        StartCoroutine(FadeDownText());
        PageController.Instance.StepAction -= FadeTextDown;

    }
    private IEnumerator FadeUpText()
    {
        yield return new WaitForSeconds(fadeUpDelay);
        if (fadeDownOnNextStar)
        {
            PageController.Instance.StepAction += FadeTextDown;
        }
        for (int i = 0; i < textToFade.Length; i++)
        {
            textToFade[i].DOFade(1, 1f);
        }
    }
    private IEnumerator FadeDownText()
    {
        yield return new WaitForSeconds(fadeDownDelay);
        for (int i = 0; i < textToFade.Length; i++)
        {
            textToFade[i].DOFade(0, 1f);
        }
    }
    public void Transition()
    {
        sparkle.Play();
        if(!fadeTextOnSelect)
        {
            FadeTextUp();
        }
        Color yellow = Color.green;
        if (magicParticles != null)
        {
            magicParticles.Play();
        }
        if (mr != null)
        {
            mrMat.color = yellow;
        }
        foreach (GameObject trail in trails)
        {
            trail.GetComponent<TrailRenderer>().material.SetColor("_EmissionColor", yellow);
        }
        SelectAction();

        PageController.Instance.Step();
        StepPage.Invoke();
        triggered = true;

        if (PageController.Instance.pageButtons.Length > 1 && PageController.Instance.starProgress < PageController.Instance.pageButtons.Length)
        {
            transform.DOMove(PageController.Instance.pageButtons[PageController.Instance.starProgress].transform.position, starSpeed);
            transform.DOScale(PageController.Instance.pageButtons[PageController.Instance.starProgress].transform.localScale, 1f);
        }
        BookController.Instance.reticle.SetBool("isTargeting", false);
    }
}
